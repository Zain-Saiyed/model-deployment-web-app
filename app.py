import os
import sys

# Flask
from flask import Flask, redirect, url_for, request, render_template, Response, jsonify, redirect
from werkzeug.utils import secure_filename
from gevent.pywsgi import WSGIServer

# TensorFlow and tf.keras
import tensorflow as tf
from keras import backend as K
#from keras.preprocessing.image import ImageDataGenerator
#from tensorflow.keras.models import load_model

# Some utilites
import numpy as np
from util import base64_to_pil
from PIL import Image
# Declare a flask app
app = Flask(__name__)
count=0
def create_model():
    from tensorflow.keras.applications.resnet import ResNet50

    qresnet_model = ResNet50(include_top=False, weights='imagenet', input_shape=(224,224,3))
    aResNet50 = tf.keras.models.Sequential()
    aResNet50.add(qresnet_model)
    aResNet50.add(tf.keras.layers.GlobalAveragePooling2D())
    aResNet50.add(tf.keras.layers.Flatten())
    aResNet50.add(tf.keras.layers.Dense(2048,activation='relu'))
    aResNet50.add(tf.keras.layers.Dropout(0.5))
    aResNet50.add(tf.keras.layers.Dense(1024,activation='relu'))
    aResNet50.add(tf.keras.layers.Dropout(0.3))
    aResNet50.add(tf.keras.layers.Dense(512,activation='relu'))
    aResNet50.add(tf.keras.layers.Dense(3,activation='softmax'))
    aResNet50.compile(loss='categorical_crossentropy',optimizer='Adam',metrics=['FalseNegatives','FalsePositives','accuracy'])

    return aResNet50

##model = create_model()
##model.load_weights('models/model_weights.h5')
global graph
global model
graph = tf.get_default_graph()
##K.clear_session()
with graph.as_default():
    model = tf.keras.models.load_model('models/best_model_84_83.h5')
##model._make_predict_function()
##model._make_predict_function()
print('Model loaded. Check http://127.0.0.1:5000/')

# Model saved with Keras model.save()
#MODEL_PATH = 'models/model.h5'
# Load your own trained model
# model = tf.keras.models.load_model(MODEL_PATH)
# model._make_predict_function()          # Necessary
# print('Model loaded. Start serving...')

@app.route('/', methods=['GET'])
def index():
    # Main page
    return render_template('index.html')

def model_predict(img, model):
    global graph
    img = img.resize((224, 224))
    #train_generator = datagen.flow_from_directory(r'C:\Users\Administrator\Pictures\keras-flask-deploy-webapp-master\keras-flask-deploy-webapp-master\uploads', target_size = (224, 224), batch_size = 1, class_mode = 'categorical')
    #X_train_orig, Y_train_orig=train_generator.next()
    x = np.array(img) *(1./255)
    print("x.shape-> ",x.shape)
    x = np.array([x])
    print("x.shape-> ",x.shape)
    with graph.as_default():
##        model = tf.keras.models.load_model('models/best_model_84_83.h5')
        Output = model.predict(x)
    print(Output)    

    return list(Output[0])

@app.route('/predict', methods=['GET', 'POST'])
def predict():
    if request.method == 'POST':
        # Get the image from post request
        img = base64_to_pil(request.json)
        global count
        count+=1
        # Save the image to ./uploads
        img.save("./uploads/image_"+str(count)+".jpg")
        x = Image.open("./uploads/image_"+str(count)+".jpg")        
        
        print("Image_name ->image_",str(count),".jpg")
        # Make prediction
        Output = model_predict(x, model)
        print("Output ->",Output)
        probability = Output.index(max(Output))
        print("probability = ",probability)
        #labels = {1:'Bathroom Mirrors',2:'Sofa',3:'Media Cabinets'}
        labels2 = {'Bathroom Mirrors': 0, 'Chaise Lounge Chairs': 1, 'Decorative Objects': 2, 'Sofas': 3, 'Vases': 4}
        labels = dict((v,k) for k,v in labels2.items())
        print("labels -> ",labels)
        result = labels[probability]
        print("result -> ", result)
        probability = str(Output[probability])
        print("probability = ",probability)
        # Serialize the result, you can add additional fields
        return jsonify(result=result , probability=probability)
    return None


if __name__ == '__main__':
##    app.run(port=5000, threaded=False,debug=True)

    # Serve the app with gevent
    http_server = WSGIServer(('127.0.0.1', 5000), app)
    http_server.serve_forever()
