# Model deployment with Flask as Web App 

[![](https://img.shields.io/badge/python-3.5%2B-green.svg)]()


## Getting started:

- Clone this repo 
- Install requirements
`$ pip install -r requirements.txt`
- Run the script
`$ python app.py`
- Go to http://localhost:5002
- Done! :tada:


<hr>